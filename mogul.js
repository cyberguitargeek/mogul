/**
 * Mogul 
 *
 * A rudimentary modal dialog for basic purposes. 
 * Author:  Chad Kelley, csharpkelley@gmail.com
 *
 * mogul.js - This file is part of the mogul project.  
 *
 *  The MIT License (MIT)
 * 
 * Copyright (c) 2014 Chad Kelley, csharpkelley@gmail.com, chadguitar@live.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
 
(function($) {
    /*
     * Global function for handling general mogul tasks like closing.  
     * @param {type} op
     * @returns {undefined}
     */
    $.mogul = function(op) {
        switch(op) {
            case "closeAndRemove":
                $.mogul.destroyAll();  
                break;
            // Alias for closeAndRemove
            case "destroy":
                $.mogul.destroyAll();  
                break;
            case "close": 
                    $(".mogul_modal").fadeOut("normal", function() {
                        $(this).hide();  
                    }); 
                break;
            default:
                break;
        }
    }
    
    $.mogul.destroyAll = function() {
         $(".mogul_modal").fadeOut("normal", function() {
            $(this).remove(); 
         });
         $(".mogul_overlay").remove(); 
    }

    $.mogul.coords = new Object();
    
    /**
     * This is a simple modal wrapper specifically for the needs of the western woods 
     * whiteboard.  
     * http://www.ericmmartin.com/projects/simplemodal/
     * by 
     * @returns {w9_modal_L1.$.fn}
     */
    $.fn.mogul = function(passIn) {
        var operation = null;
        if (typeof passIn === "string") {
            operation = passIn; 
        }
        if (!operation) {
            $(this).each(function() {
                var defaults = {
                    contents: '',
                    id: 'mogul' + Math.random().toString(36).substring(7),
                    position: null,
                    zIndex: 999999, 
                    title: '',
                    modal: true,
                    centerToMouse: false,
                    width: false  
                };
				// Merge settings.
                var settings = $.extend(defaults, passIn); 
                // Overlay markup
                var overlayMarkup = "<div class=\"mogul_overlay\"></div>"; 
                // Modal markup  
                var modalMarkup = '<div id="' + settings.id + '" class="mogul mogul_modal">' + 
                                      '<div id="' + settings.id + '_controls" class="mogul_controls">' +
                                          '<div class="mogul_close">&nbsp;</div>' + 
                                          '<div class="mogul_title">' + settings.title + '</div>' + 
                                      '</div>' +
                                      '<div class="mogul_messages" id="' + settings.id + '_messages"> </div>' +
                                      '<div class="mogul_body" id="' + settings.id + '_body" class="no_width">' +
                                      '</div>' +
                                  '</div>'; 
				
                // Display the modal and handle events. 
                if ($("#" + settings.id).length === 0) {
					
					// This is a makeshift way of getting through a race condition that happens
					// when a mogul link is clicked that uses the same div, when a mogul is
					// already visible. If the action of showing is allowed to push through prior to 
					// delete completion, the element that the user desires to be shown is removed from
					// the DOM. 
					// FIXME: Fix the race condition in a better way? 
					var origParent = null;
					
					// If there is a race condition and the old modal is being hidden while the current modal
					// is being shown, the content layer will get removed because it hasn't been properly moved
					// to its parent node
					// FIXME: Since checking for a race condition is so specific, we may want to rename classes to
					// omit any possibility of naming conflicts.  
					if ($(this).parent().hasClass("mogul_body")) {
						var my_unfinished_layer = this;
						setTimeout(function() {
							$(my_unfinished_layer).mogul(settings);
						}, 45);
						return; 
					} else {
						// Get the original location of the selector.  
						origParent = $(this).parent();	
					}
					
					$("body").append(modalMarkup); 
                    var el = $("#" + settings.id); 
                    el.css({
                        zIndex: settings.zIndex
                    });
					$(this).show();
                    
                    // Add content.  
                    if (settings.contents != '') {
                        el.find('.mogul_body').html(settings.contents); 
                    } else {
                        el.find('.mogul_body').html($(this));
                    }
                    
                    if (settings.width) {
                        el.css("width", settings.width + "px");   
                    }
                        
                    Mogul.createEvents(settings, el, $(this), overlayMarkup, origParent); 
                    Mogul.adjustDimensions(settings, el, this); 
                    $("#" + settings.id).show();
                } 
            }); 
        } else {
            if (operation == "close") {
                $(".mogul_overlay").remove(); 
                $(".mogul").remove(); 
            }
        }
        return this;
    };

    /*
     * A W9Modal namespace object.  
     * FIXME: Use an object to pass in the additional params for the sake of cleaning up the 
     *        syntax.  
     */
    var Mogul = {
        createEvents: function(settings, el, contEl, overlayMarkup, parent) {
              var over = $(overlayMarkup);
              // Display the overlay.  
              if (settings.modal) {
                  // Show the overlay
                  over.css({
                      zIndex: settings.zIndex - 1 
                  }); 
                  $("body").append(over);
              }     
              Mogul.handleModalEvents(settings, over, el, contEl, parent);
        }, 
        adjustDimensions: function(settings, wrapperEl) {
            // If they pass in a position use that one
            if (settings.position) {
               wrapperEl.position(settings.position); 
            // If no position is passed in create one. 
            } else {
                var centerOffsetX = 0;
                var centerOffsetY = 0; 
                if (settings.centerToMouse) {
                  centerOffsetX = parseInt($(wrapperEl).width()/2);
                  centerOffsetY = parseInt($(wrapperEl).height()/2);                           
                } 
                wrapperEl.offset({
                     top: $.mogul.coords.y - centerOffsetY,
					 left: (($.mogul.coords.x - centerOffsetX) < 2) ? 2 : $.mogul.coords.x - centerOffsetX
                }); 
            }
            // Adjust clipping on the right side of the screen.  
            Mogul.adjustClipping(wrapperEl);  
        },
        handleModalEvents: function(settings, over, el, contEl, parent) {
            // Handle the events 
            el.find('.mogul_close').click(function() {
                over.remove();
                contEl.hide(); 
                parent.append(contEl); 
                el.remove(); 
            }); 
            
            if (settings.modal) {
                over.one('click', function() {
                    over.remove();
                	parent.append(contEl); 									
					contEl.hide(); 
                    el.remove(); 
                });  
            } else {
				// Create a function that can be unbound. 
				var bodClickAction = function(evt) {
		            var inYRange = evt.pageY < el.position().top || evt.pageY > el.position().top + el.outerHeight();
		            var inXRange = evt.pageX < el.position().left || evt.pageX > el.position().left + el.outerWidth();
		            if (inYRange || inXRange) {
		                over.remove();  
                    	contEl.hide(); 
						parent.append(contEl); 	
					    el.remove();  
						$("body").unbind('click', clickActionWrapper); 
		             }
				}; 
				var clickActionWrapper = function(evt) {
					bodClickAction(evt);
				}; 
                $("body").on("click", clickActionWrapper);
            }
        }, 
		
        /*
         * Don't let the modal clip off of the page. 
         */
        adjustClipping: function(el) {
            if (el.position().left + el.width() > $(window).innerWidth()) {
                el.css({
                    left: $(window).innerWidth() - el.width() - 5
                }); 
            }
        }
    }; 

}(jQuery));

// Track mouse coordinates. 
$(document).ready(function() {
    $("body").mousemove(function(evt) {
        $.mogul.coords = {
            x: evt.pageX,
            y: evt.pageY
        };
    });
});

